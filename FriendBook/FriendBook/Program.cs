using FriendBook.Persistence.Context;
using FriendBook.Persistence.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder
    .Services
    .AddIdentity<User, IdentityRole<int>>(options =>
     {
         options.Password.RequireDigit = false;
         options.Password.RequireNonAlphanumeric = false;
         options.Password.RequireUppercase = false;
         options.Password.RequireLowercase = false;

         options.User.RequireUniqueEmail = true;
         options.SignIn.RequireConfirmedEmail = false; // TODO: confirm email
     })
    .AddEntityFrameworkStores<FriendBookDbContext>()
    .AddDefaultTokenProviders();

builder
    .Services
    .AddDbContext<FriendBookDbContext>(options =>
     {
         options.UseSqlServer(builder.Configuration.GetConnectionString("FriendbookDB"));
     });

var app = builder.Build();


app.Run();
