﻿using FriendBook.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FriendBook.Persistence.Configurations
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(64);

            builder
                .Property(b => b.Description)
                .HasMaxLength(264);

            builder
                .Property(b => b.Language)
                .HasMaxLength(24);
        }
    }
}
