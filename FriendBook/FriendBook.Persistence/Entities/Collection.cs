﻿namespace FriendBook.Persistence.Entities
{
    public class Collection
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; } = null!;
        public string Name { get; set; } = null!;
        public ICollection<Book>? Books { get; set; }
    }
}
