﻿namespace FriendBook.Persistence.Entities
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public ICollection<Book>? Books { get; set; }
    }
}
