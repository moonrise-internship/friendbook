﻿namespace FriendBook.Persistence.Entities
{
    public class Bookmark
    {
        public int BookId { get; set; }
        public Book Book { get; set; } = null!;
        public int UserId { get; set; }
        public User User { get; set; } = null!;
        public int PageNumber { get; set; }
    }
}
