﻿namespace FriendBook.Persistence.Entities
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public string Language { get; set; } = null!;
        public int NumberOfPages { get; set; }
        public ICollection<Genre> Genres { get; set; } = null!;
        public ICollection<Author>? Authors { get; set; }
        public ICollection<Collection>? Collections { get; set; }
    }
}
