﻿using Microsoft.AspNetCore.Identity;

namespace FriendBook.Persistence.Entities
{
    public class User : IdentityUser<int>
    {
        public ICollection<Collection>? Collections { get; set; }
    }
}
