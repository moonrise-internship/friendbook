﻿namespace FriendBook.Persistence.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; } = null!;
        public int UserId { get; set; }
        public User? User { get; set; }
        public string Text { get; set; } = null!;
        public DateTime DateTime { get; set; }
    }
}
