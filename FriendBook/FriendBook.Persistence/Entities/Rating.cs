﻿namespace FriendBook.Persistence.Entities
{
    public class Rating
    {
        public int BookId { get; set; }
        public int UserId { get; set; }
        public Book Book { get; set; } = null!;
        public User User { get; set; } = null!;
        public bool IsRecommended { get; set; }
    }
}
