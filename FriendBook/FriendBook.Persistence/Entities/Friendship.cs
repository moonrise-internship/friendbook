﻿namespace FriendBook.Persistence.Entities
{
    public class Friendship
    {
        public int UserId { get; set; }
        public int FriendId { get; set; }
        public User User { get; set; } = null!;
        public User Friend { get; set; } = null!;
    }
}
