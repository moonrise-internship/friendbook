﻿namespace FriendBook.Persistence.Entities
{
    public class Message
    {
        public int Id { get; set; }
        public int SenderId { get; set; }
        public User Sender { get; set; } = null!;
        public int GetterId { get; set; }
        public User Getter { get; set; } = null!;
        public string Text { get; set; } = null!;
        public DateTime DateTime { get; set; }
    }
}
