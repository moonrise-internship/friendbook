﻿using FriendBook.Persistence.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FriendBook.Persistence.Context
{
    public class FriendBookDbContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public FriendBookDbContext(DbContextOptions<FriendBookDbContext> options) : base(options)
        {
        }

        public DbSet<Author> Authors { get; set; } = null!;
        public DbSet<Book> Books { get; set; } = null!;
        public DbSet<Bookmark> Bookmarks { get; set; } = null!;
        public DbSet<Collection> Collections { get; set; } = null!;
        public DbSet<Comment> Comments { get; set; } = null!;
        public DbSet<Genre> Genres { get; set; } = null!;
        public DbSet<Message> Messages { get; set; } = null!;
        public DbSet<Rating> Ratings { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(FriendBookDbContext).Assembly);
            base.OnModelCreating(builder);
        }

    }
}
